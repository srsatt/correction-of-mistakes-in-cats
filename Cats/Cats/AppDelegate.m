//
//  AppDelegate.m
//  Cats
//
//  Created by Artemiy on 29.09.14.
//  Copyright (c) 2014 Artemiy. All rights reserved.
//

#import "AppDelegate.h"
#import "Cat.h"
//1 ошибка - нету импорта
@interface AppDelegate ()
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [self allocatingExampleCats];
    [self testStoringCats];
}


- (void)testStoringCats{
    Cat *cat = [[Cat alloc] initWithNumberOfColors:3];
    cat.isMale = YES;
    
    NSData *catInData = [NSKeyedArchiver archivedDataWithRootObject:cat];
    Cat *extractedCat = [NSKeyedUnarchiver unarchiveObjectWithData:catInData];
   
    assert([cat isEqualToCat:extractedCat]);
    
    assert(cat.isMale == NO);
    // FIXME: вообще-то нет, у нас из примера мужика-кошки 3х цветов не может быть, после cat.isMale = YES на самом деле должно быть cat.isMale == NO
    //fixed: переписал в классе. Я немного строки перепутал, и подумал, что при архивировании/разорхивировании кошки у нас внезапно должен был смениться пол
}


- (void)allocatingExampleCats{
    Cat *another_cat = [[Cat alloc] init];
    //2 ошибка - потерялся инит
    assert(another_cat.numberOfColors);
    NSLog(@"%@", another_cat);
    
    
    Cat *my_cat = [[Cat alloc] init];
    my_cat.nickName = @"murzik";
    NSLog(@"%@", my_cat);
}


@end
