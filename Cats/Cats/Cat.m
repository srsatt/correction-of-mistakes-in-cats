//
//  Cat.m
//  FirstLesson
//
//  Created by artemiy on 13.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Cat.h"
#import "Animal.h"
NSString * const CatNickNameKey = @"cat nick name";
NSString * const CatNumberOfColorsKey = @"cat number of colors";


@implementation Cat

#pragma mark - Initialisation
- (instancetype)initWithNumberOfColors:(int)numberOfColors{
    if (numberOfColors > 3 || numberOfColors < 1) return nil;
    if (!(self = [self init]))  return nil;
    _numberOfColors = numberOfColors;
    _nickName=[Cat defaultCatName];
    //3 ошибка - кошку дефолтно должны как-то звать
    return self;
}


- (instancetype)init{
    if (!(self = [super init]))  return nil;
    _nickName=[Cat defaultCatName];
    _numberOfColors=1;
    //3 ошибка - кошку дефолтно должны как-то звать
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (!(self = [super initWithCoder:aDecoder])) return nil;
    _nickName = [aDecoder decodeObjectForKey:CatNickNameKey];
    _numberOfColors = [aDecoder decodeIntForKey:CatNumberOfColorsKey];
    //[super setIsMale:[aDecoder decodeBoolForKey:@"gender"]];
    
    return self;
}

#pragma mark - saving
- (void)encodeWithCoder:(NSCoder *)aCoder{
    //FIXED: ну для этого нужно сделать так:
    [super encodeWithCoder:aCoder];
    
    [aCoder encodeObject:self.nickName forKey:CatNickNameKey];
    // 4 ошибка -  не тот кей
    [aCoder encodeInteger:self.numberOfColors forKey:CatNumberOfColorsKey];
//  FIXME: вообще-то нет - посмотрите в Animal
    
    //[aCoder encodeBool:self.isMale forKey: @"gender"];
    //согласен, что способ выше правильнее, но в конкретном случае это работало
    //5 ошибка - пропущено сохранение пола кошки
    
}

#pragma mark - model

- (void)setNumberOfColors:(int)numberOfColors{
    if (numberOfColors > 3 || numberOfColors < 1) {
        return;
    }
    else if (numberOfColors == 3 && self.isMale){
        return;
    }
    _numberOfColors = numberOfColors;
}


- (BOOL)isEqualToCat:(Cat *)cat{
    if (self == cat) return YES;
    if (self.isMale != cat.isMale) return NO;
    if (![self.nickName isEqualToString: cat.nickName]) return NO;
    //7 ошибка - строковое сравнение
    if (self.numberOfColors != cat.numberOfColors) return NO;
    return YES;
}

#pragma mark - other methods

+ (NSString *)defaultCatName{
    return @"murmur";
}
-(void)setIsMale:(BOOL)isMale{
    if(isMale==YES && self.numberOfColors==3) [super setIsMale:NO];
    else [super setIsMale:YES];
        }
- (NSString *)description{
    assert(self.nickName);
    return [NSString stringWithFormat:@"cat name:%@ %d %d", self.nickName, self.isMale, self.numberOfColors];
}

@end
